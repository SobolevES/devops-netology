--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- 
--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- 

**Задача автоматизировать установку и конфигурацию ПО:**  
  
-установить Java, Elasticsearch, Kibana, Logstash  
-добавить переменные окружения  
-конфигурация Elasticsearch  
-конфигурация Kibana  
-Конфигурация Logstash  
  

**inventory:**  
  
192.168.100.11, Ubuntu 20.04 -- control node, с которой запускаем плейбук  
192.168.100.33, Ubuntu 20.04 -- managed node, на которую установим Java, Elasticsearch, Kibana  
192.168.100.66, Ubuntu 20.04 -- managed node, на которую установим Logstash  
  
**files:**  
  
Для единообразия проекта были скачаны все дистрибы  
elasticsearch-7.13.2-linux-x86_64.tar.gz  
jdk-11.0.11_linux-x64_bin.tar.gz  
kibana-7.13.2-linux-x86_64.tar.gz  
logstash-7.13.2-linux-x86_64.tar.gz  
  

**Запуск плейбука**
  
`ansible-playbook -i inventory/prod.yml --ask-pass site.yml`  

Результат первого запуска был с фейлом:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/1_10_ansible2.JPG)  
  
Исправил ошибку, повторил запуск:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/2_10_ansible2.JPG)  
  
И еще раз:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/3_10_ansible2.JPG)  
  

**А теперь запуск и проверка:**  
  
эластик:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/4_10_ansible2.JPG)  
  
кибана:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/5_10_ansible2.JPG)  
  
логстеш:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/6_10_ansible2.JPG)  
  
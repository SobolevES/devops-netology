##Домашнее задание к занятию "11.04 Микросервисы: масштабирование"
___
Вы работаете в крупной компанию, которая строит систему на основе микросервисной архитектуры. Вам как DevOps специалисту необходимо выдвинуть предложение по организации инфраструктуры, для разработки и эксплуатации.

###Задача 1: Кластеризация  
___
Предложите решение для обеспечения развертывания, запуска и управления приложениями. Решение может состоять из одного или нескольких программных продуктов и должно описывать способы и принципы их взаимодействия.
Решение должно соответствовать следующим требованиям:  
  
* Поддержка контейнеров;  
* Обеспечивать обнаружение сервисов и маршрутизацию запросов;  
* Обеспечивать возможность горизонтального масштабирования;  
* Обеспечивать возможность автоматического масштабирования;  
* Обеспечивать явное разделение ресурсов доступных извне и внутри системы;  
* Обеспечивать возможность конфигурировать приложения с помощью переменных среды, в том числе с возможностью безопасного хранения чувствительных данных таких как пароли, ключи доступа, ключи шифрования и т.п.  
  
Обоснуйте свой выбор.

====
  
### Решение:  
  
Хотелось бы придумать что то свое, но уже тысяча статей про сравнение инструментов контейнеризации.  
Вот табличка, которая показывает плюсы и минусы того или иного инструмента.  

✓✓✓ — самая удачная и удобная в использовании реализация;  
✓✓ — удовлетворительная реализация, уступающая другим по ряду параметров;  
✓ — реализация, уступающая другим по большинству параметров и/или требующая значительно больше времени и ресурсов для ее использования.  

![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/micro4.JPG)  
  
Судя по табличке, кубер - лидирующий инструмент по многим показателям, хотя и имеет недостатки, например высокий порог входа для новых пользователей при выборе ручной установки, хотя и эта сложность решаема.  
  
Из личного опыта могу сказать, что я работал только с кубером и несмотря на то, что его сейчас внедряют везде где надо и не надо - инструмент действительно удобный, хотя и не самый простой.  
Для себя выделяю плюсы кубера: отказоустойчивость (в случае падения ноды в кластере, поды поедут на другую свободную ноду), возможность разносить функционал на разные поды (например, интеграцию), легкая интеграция с CI/CD (с Jenkins настроить интеграцию для обновления приложения), визуально удобно работать с подами.  
  
Мой выбор очевиден :)  


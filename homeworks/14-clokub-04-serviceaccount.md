# Домашнее задание к занятию "14.4 Сервис-аккаунты"

## Задача 1: Работа с сервис-аккаунтами через утилиту kubectl в установленном minikube

Выполните приведённые команды в консоли. Получите вывод команд. Сохраните
задачу 1 как справочный материал.

### Как создать сервис-аккаунт?

```
kubectl create serviceaccount netology
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.4-create.servacc.png)  
  
### Как просмотреть список сервис-акаунтов?

```
kubectl get serviceaccounts
kubectl get serviceaccount
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.4-get.serviceaccount.png)  
  
### Как получить информацию в формате YAML и/или JSON?

```
kubectl get serviceaccount netology -o yaml
kubectl get serviceaccount default -o json
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.4-jaml&json.png)  
  
### Как выгрузить сервис-акаунты и сохранить его в файл?

```
kubectl get serviceaccounts -o json > serviceaccounts.json
kubectl get serviceaccount netology -o yaml > netology.yml
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.4->.png)  
  
### Как удалить сервис-акаунт?

```
kubectl delete serviceaccount netology
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.4-delete.png)  
  
### Как загрузить сервис-акаунт из файла?

```
kubectl apply -f netology.yml
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.4-apply.png)  
  
## Задача 2 (*): Работа с сервис-акаунтами внутри модуля

Выбрать любимый образ контейнера, подключить сервис-акаунты и проверить
доступность API Kubernetes

```
kubectl run -i --tty fedora --image=fedora --restart=Never -- sh
```

Просмотреть переменные среды

```
env | grep KUBE
```

Получить значения переменных

```
K8S=https://$KUBERNETES_SERVICE_HOST:$KUBERNETES_SERVICE_PORT
SADIR=/var/run/secrets/kubernetes.io/serviceaccount
TOKEN=$(cat $SADIR/token)
CACERT=$SADIR/ca.crt
NAMESPACE=$(cat $SADIR/namespace)
```

Подключаемся к API

```
curl -H "Authorization: Bearer $TOKEN" --cacert $CACERT $K8S/api/v1/
```

В случае с minikube может быть другой адрес и порт, который можно взять здесь

```
cat ~/.kube/config
```

или здесь

```
kubectl cluster-info
```

---
### Решение:  
вопросов не возникло, все подробно описано )  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.4-2.1.png)  
    
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.4-2.2.png)  
  
### Как оформить ДЗ?

Выполненное домашнее задание пришлите ссылкой на .md-файл в вашем репозитории.

В качестве решения прикрепите к ДЗ конфиг файлы для деплоя. Прикрепите скриншоты вывода команды kubectl со списком запущенных объектов каждого типа (pods, deployments, serviceaccounts) или скриншот из самого Kubernetes, что сервисы подняты и работают, а также вывод из CLI.

---

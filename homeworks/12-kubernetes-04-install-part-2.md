# Домашнее задание к занятию "12.4 Развертывание кластера на собственных серверах, лекция 2"
Новые проекты пошли стабильным потоком. Каждый проект требует себе несколько кластеров: под тесты и продуктив. Делать все руками — не вариант, поэтому стоит автоматизировать подготовку новых кластеров.

## Задание 1: Подготовить инвентарь kubespray
Новые тестовые кластеры требуют типичных простых настроек. Нужно подготовить инвентарь и проверить его работу. Требования к инвентарю:
* подготовка работы кластера из 5 нод: 1 мастер и 4 рабочие ноды;
* в качестве CRI — containerd;
* запуск etcd производить на мастере.

___
### Решение:  

Первым делом поднял виртуалку **datacentr**, подключился к ней по ssh, сгенерил ключ rsa и далее уже приватный ключ использовал для всех остальных VM.  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber4.2_vm.png)   
  
Переписывать все команды не буду, напишу главное (больше для себя) - это репу для клонирования  
`git clone git@github.com:kubernetes-sigs/kubespray.git`  
ридмишка куберспрея в помощь.  

Итак, мой ямлик  `evgeniy@datacentr:/opt/kubespray$ cat inventory/mycluster/hosts.yaml`:  

```yaml
all:
  hosts:
    master1:
      ansible_host: 10.128.0.6
      ip: 10.128.0.6
      access_ip: 10.128.0.6
    node1:
      ansible_host: 10.128.0.17
      ip: 10.128.0.17
      access_ip: 10.128.0.17
    node2:
      ansible_host: 10.128.0.36
      ip: 10.128.0.36
      access_ip: 10.128.0.36
    node3:
      ansible_host: 10.128.0.25
      ip: 10.128.0.25
      access_ip: 10.128.0.25
    node4:
      ansible_host: 10.128.0.18
      ip: 10.128.0.18
      access_ip: 10.128.0.18
  children:
    kube_control_plane:
      hosts:
        master1:
    kube_node:
      hosts:
        master1:
        node1:
        node2:
        node3:
        node4:
    etcd:
      hosts:
        master1:
    k8s_cluster:
      children:
        kube_control_plane:
        kube_node:
    calico_rr:
      hosts: {}
```

Далее **первый запуск** плейбука(опять пишу больше для себя^^  ):  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber4.2_playbook_FAIL1.png)   
  
Визуально ошибка понятная, но что делать не совсем понятно... Долго не думая я подключился на мастера и попробовал выполнить команду `apt update` и что же я увидел?...  
Сервак ломится на внешку, чтобы слить с репозиториев обновы, а т.к. при создании виртуалки я отключил публичный адрес, то проблему быстро удалось решить путем пересоздания виртуалок (я вспомнил что на лекции это упоминалось).
  

Далее после исправлений **второй запуск** плейбука:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber4.2_playbook_FAIL2.png)   
  
Ошибка вообще не понятно почему возникла, но я докинул прав дире, на которую он сругнулся.  

И наконец финал - **третий запуск** плейбука:  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber4.2_playbook_OK.png)   
  
**Успех!**  

Подключаемся на мастера и проверяем кластер:  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber4.2_nodes.png)   
  
  

## Домашнее задание к занятию "10.02. Системы мониторинга"
### Обязательные задания

1. Опишите основные плюсы и минусы pull и push систем мониторинга.

### **Ответ:**  

push:  
`+` гибкая настройка отправки данных с метриками  
`+` проще настроить бэкап  
`+` отправка данных в несколько систем мониторинга  
`+` работа по протолоку UDP, что повысит скорость  
`-` сложный дебаг  
`-` потеря данных, если будем юзать UDP  
`-` сложно контролировать подлинность данных  
pull:    
`+` единая точка конфигурирования  
`+` легче производить отладку  
`+` легче контролировать достоверность данных  
`-` необходимо открыть сетевой доступ с цетрального узла до агентов  
`-` в случае отвала сети мы теряем показатели за время её простоя  




2. Какие из ниже перечисленных систем относятся к push модели, а какие к pull? А может есть гибридные?
- Prometheus
- TICK
- Zabbix
- VictoriaMetrics
- Nagios
  
### **Ответ:**  
- Prometheus - pull
- TICK - Push
- Zabbix - Гибрид
- VictoriaMetrics - Гибрид
- Nagios -Гибрид
  

3. Склонируйте себе репозиторий и запустите TICK-стэк, используя технологии docker и docker-compose.

В виде решения на это упражнение приведите выводы команд с вашего компьютера (виртуальной машины):

- curl http://localhost:8086/ping
- curl http://localhost:8888
- curl http://localhost:9092/kapacitor/v1/ping

А также скриншот веб-интерфейса ПО chronograf (http://localhost:8888).  
  
P.S.: если при запуске некоторые контейнеры будут падать с ошибкой - проставьте им режим Z, например ./data:/var/lib:Z
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/monitoring2_curl.png)   
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/monitoring2_ui_chronograf.png)  
  
4. Перейдите в веб-интерфейс Chronograf (http://localhost:8888) и откройте вкладку Data explorer.
        - Нажмите на кнопку Add a query
        - Изучите вывод интерфейса и выберите БД telegraf.autogen
        - В measurments выберите mem->host->telegraf_container_id , а в fields выберите used_percent. Внизу появится график утилизации оперативной памяти в контейнере telegraf.
        - Вверху вы можете увидеть запрос, аналогичный SQL-синтаксису. Поэкспериментируйте с запросом, попробуйте изменить группировку и интервал наблюдений.

Для выполнения задания приведите скриншот с отображением метрик утилизации места на диске (disk->host->telegraf_container_id) из веб-интерфейса.
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/monitoring2_mem.png)  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/monitorin2_disk.png)  
  
5. Изучите список telegraf inputs. Добавьте в конфигурацию telegraf следующий плагин - docker:

```
[[inputs.docker]]
  endpoint = "unix:///var/run/docker.sock"
```  

Дополнительно вам может потребоваться донастройка контейнера telegraf в docker-compose.yml дополнительного volume и режима privileged:
```
  telegraf:
    image: telegraf:1.4.0
    privileged: true
    volumes:
      - ./etc/telegraf.conf:/etc/telegraf/telegraf.conf:Z
      - /var/run/docker.sock:/var/run/docker.sock:Z
    links:
      - influxdb
    ports:
      - "8092:8092/udp"
      - "8094:8094"
      - "8125:8125/udp"
```
После настройке перезапустите telegraf, обновите веб интерфейс и приведите скриншотом список measurments в веб-интерфейсе базы telegraf.autogen . Там должны появиться метрики, связанные с docker.
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/monitoring2_docker.png)  
  
Факультативно можете изучить какие метрики собирает telegraf после выполнения данного задания.
## Дополнительное задание (со звездочкой*) - необязательно к выполнению

В веб-интерфейсе откройте вкладку Dashboards. Попробуйте создать свой dashboard с отображением:

- утилизации ЦПУ
- количества использованного RAM
- утилизации пространства на дисках
- количество поднятых контейнеров
- аптайм
- ...
- фантазируйте)

---
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/monitorin2_dashboard.png)  

### Как оформить ДЗ?

Выполненное домашнее задание пришлите ссылкой на .md-файл в вашем репозитории.
# Домашнее задание к занятию "12.5 Сетевые решения CNI"
После работы с Flannel появилась необходимость обеспечить безопасность для приложения. Для этого лучше всего подойдет Calico.
## Задание 1: установить в кластер CNI плагин Calico
Для проверки других сетевых решений стоит поставить отличный от Flannel плагин — например, Calico. Требования: 
* установка производится через ansible/kubespray;
* после применения следует настроить политику доступа к hello world извне.
  
### Решение:  
Делаем по аналогии с прошлой дз.  
Поднимаем ВМ (тут я урезал на 2 машины ибо "итак сойдет"): будет 1 мастер и 2 воркера:  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber12.5_vm.png)   
  
```yaml
all:
  hosts:
    master1:
      ansible_host: 10.128.0.19
      ip: 10.128.0.19
      access_ip: 10.128.0.19
    node1:
      ansible_host: 10.128.0.11
      ip: 10.128.0.11
      access_ip: 10.128.0.11
    node2:
      ansible_host: 10.128.0.7
      ip: 10.128.0.7
      access_ip: 10.128.0.7
  children:
    kube_control_plane:
      hosts:
        master1:
    kube_node:
      hosts:
        master1:
        node1:
        node2:
    etcd:
      hosts:
        master1:
    k8s_cluster:
      children:
        kube_control_plane:
        kube_node:
    calico_rr:
      hosts: {}
```
  
Стартуем плейбук:  
`evgeniy@datacentr:/opt/kubespray$ ansible-playbook -i inventory/mycluster/hosts.yaml -e kube_network_plugin=calico cluster.yml -v -b`  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber12.5_playbook_OK.png)   
  

Коннектимся к мастер-ноде `ssh evgeniy@10.128.0.19`  и проверяем ноды кластера кубера:  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber12.5_nodes.png)   
  
Чтобы открыть возможность работать с сервисом снаружи, нам нужен балансир, по мануалу делаем  https://kubernetes.github.io/ingress-nginx/deploy/baremetal/#a-pure-software-solution-metallb  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber12.5_balancer.png)   
  
Правим конфигмап, добавив в него внешние айпишники:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber12.5_configmap.png)   
  
Создаем деплоймент *hello world* :  

![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber12.5_hello_deployment.png)   
  
Далее создадим конфиг для сервиса:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber12.5_serv_hello.png)   

и проврим, что сервис создался:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber12.5_get_service.png)   
  
Проверяем, что получилось со строны клиента:  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber12.5_check_url1.png)  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber12.5_check_url2.png)   
  
Урлы доступы с локальной машины.  
Повесим теперь политику запрета. Разрешим подключаться к сервису только с одного хоста и это будет мастер/  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber12.5_allow_trafic.png)   
  
Проверяем:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber12.5_check_url_allow.png)   
Отлично, с локала доступ закрыт, значит и с других хостов тоже.  
Проверим работу с разрешенного хоста:  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber12.5_curl.png)   
Success!  
  


## Задание 2: изучить, что запущено по умолчанию
Самый простой способ — проверить командой calicoctl get <type>. Для проверки стоит получить список нод, ipPool и profile.
Требования: 
* установить утилиту calicoctl;
* получить 3 вышеописанных типа в консоли.

___
### Решение:  
Утилиту я не ставил дополнительно, за меня это сделал куберспрей)  
  
Чекаем:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber12.5_nodes_pool.png)   
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber12.5_profiles.png)   
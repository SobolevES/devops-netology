# Домашнее задание к занятию "14.1 Создание и использование секретов"

## Задача 1: Работа с секретами через утилиту kubectl в установленном minikube

Выполните приведённые ниже команды в консоли, получите вывод команд. Сохраните
задачу 1 как справочный материал.

### Как создать секрет?

```
openssl genrsa -out cert.key 4096
openssl req -x509 -new -key cert.key -days 3650 -out cert.crt \
-subj '/C=RU/ST=Moscow/L=Moscow/CN=server.local'
kubectl create secret tls domain-cert --cert=certs/cert.crt --key=certs/cert.key
```
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.1-openssl.png)   
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.1-create.png)   
  
### Как просмотреть список секретов?

```
kubectl get secrets
kubectl get secret
```  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.1-get.png)   
  
### Как просмотреть секрет?

```
kubectl get secret domain-cert
kubectl describe secret domain-cert
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.1-describe.png)   
  
### Как получить информацию в формате YAML и/или JSON?

```
kubectl get secret domain-cert -o yaml
kubectl get secret domain-cert -o json
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.1-json.png)   
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.1-yaml.png)   
  
  
### Как выгрузить секрет и сохранить его в файл?

```
kubectl get secrets -o json > secrets.json
kubectl get secret domain-cert -o yaml > domain-cert.yml
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.1>.png)   
  
### Как удалить секрет?

```
kubectl delete secret domain-cert
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.1-delete.png)   
  
### Как загрузить секрет из файла?

```
kubectl apply -f domain-cert.yml
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.1-apply.png)   
  

## Задача 2 (*): Работа с секретами внутри модуля

Выберите любимый образ контейнера, подключите секреты и проверьте их доступность
как в виде переменных окружения, так и в виде примонтированного тома.

---

### Как оформить ДЗ?

Выполненное домашнее задание пришлите ссылкой на .md-файл в вашем репозитории.

В качестве решения прикрепите к ДЗ конфиг файлы для деплоя. Прикрепите скриншоты вывода команды kubectl со списком запущенных объектов каждого типа (deployments, pods, secrets) или скриншот из самого Kubernetes, что сервисы подняты и работают, а также вывод из CLI.

---

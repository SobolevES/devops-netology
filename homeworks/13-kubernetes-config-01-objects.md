# Домашнее задание к занятию "13.1 контейнеры, поды, deployment, statefulset, services, endpoints"
Настроив кластер, подготовьте приложение к запуску в нём. Приложение стандартное: бекенд, фронтенд, база данных (пример можно найти в папке 13-kubernetes-config).

## Задание 1: подготовить тестовый конфиг для запуска приложения
Для начала следует подготовить запуск приложения в stage окружении с простыми настройками. Требования:
* под содержит в себе 3 контейнера — фронтенд, бекенд, базу;
* регулируется с помощью deployment фронтенд и бекенд;
* база данных — через statefulset.

___
### Решение:  
  
Я собрал образы не через докер-компоус, как предлагается в ридмишки, а просто через докер:  
  
```
cd ../13-kubernetes-config/
docker build backend/ -t soboleves/homework-backend:stage
docker build frontend/ -t soboleves/homework-fronend:stage
docker login
docker push soboleves/homework-backend:stage
docker push soboleves/homework-fronend:stage
```
  
Далее подготовил манифест для стейджа:  
  
**stage.yaml**
````yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: front-back
  labels:
    app: front-back
spec:
  replicas: 1
  selector:
    matchLabels:
      app: front-back
  template:
    metadata:
      labels:
        app: front-back
    spec:
      containers:
      - name: frontend
        image: soboleves/homework-fronend:stage
        ports:
        - containerPort: 80
      - name: backend
        image: soboleves/homework-backend:stage
        ports:
        - containerPort: 9000
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: db
spec:
  selector:
    matchLabels:
      app: db
  serviceName: db
  replicas: 1
  template:
    metadata:
      labels:
        app: db
    spec:
      terminationGracePeriodSeconds: 10
      containers:
      - name: db
        image: postgres:13-alpine
        ports:
        - containerPort: 5432
        env:
          - name: POSTGRES_PASSWORD
            value: postgres
          - name: POSTGRES_USER
            value: postgres
          - name: POSTGRES_DB
            value: news
---
apiVersion: v1
kind: Service
metadata:
  name: db
spec:
  selector:
    app: db
  ports:
    - protocol: TCP
      port: 5432
      targetPort: 5432
````
  
Стартуем:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber13.1_apply_stage.png)   
   



## Задание 2: подготовить конфиг для production окружения
Следующим шагом будет запуск приложения в production окружении. Требования сложнее:
* каждый компонент (база, бекенд, фронтенд) запускаются в своем поде, регулируются отдельными deployment’ами;
* для связи используются service (у каждого компонента свой);
* в окружении фронта прописан адрес сервиса бекенда;
* в окружении бекенда прописан адрес сервиса базы данных.

___
### Решение:  
Для прод окружения изменил параметры в исходниках и пересобрал образы:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber13.1_build_prod.png)   
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber13.1_docker_images.png)
  
Запушил:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber13.1_docker_push_prod.png)   
  
Далее изменил манифест, разделив деплоймент и добавим сервисы:  
**prod.yaml**
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: frontend
  labels:
    app: frontend
spec:
  replicas: 1
  selector:
    matchLabels:
      app: frontend
  template:
    metadata:
      labels:
        app: frontend
    spec:
      containers:
      - name: frontend
        image: soboleves/homework-frontend:prod
        ports:
        - containerPort: 80
        env:
          - name: BASE_URL
            value: http://localhost:9000
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: backend
  labels:
    app: backend
spec:
  replicas: 1
  selector:
    matchLabels:
      app: backend
  template:
    metadata:
      labels:
        app: backend
    spec:
      containers:
      - name: backend
        image: soboleves/homework-backend:prod
        ports:
        - containerPort: 9000
        env:
          - name: DATABASE_URL
            value: postgres://postgres:postgres@application-db-prod:5432/news
        
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: db
spec:
  selector:
    matchLabels:
      app: db
  serviceName: db
  replicas: 1
  template:
    metadata:
      labels:
        app: db
    spec:
      terminationGracePeriodSeconds: 10
      containers:
      - name: db
        image: postgres:13-alpine
        ports:
        - containerPort: 5432
        env:
          - name: POSTGRES_PASSWORD
            value: postgres
          - name: POSTGRES_USER
            value: postgres
          - name: POSTGRES_DB
            value: news
---
apiVersion: v1
kind: Service
metadata:
  name: frontend
spec:
  selector:
    app: frontend
  ports:
    - protocol: TCP
      port: 8000
      targetPort: 80
---
apiVersion: v1
kind: Service
metadata:
  name: backend
spec:
  selector:
    app: backend
  ports:
    - protocol: TCP
      port: 9000
      targetPort: 9000
---
apiVersion: v1
kind: Service
metadata:
  name: db
spec:
  selector:
    app: db
  ports:
    - protocol: TCP
      port: 5432
      targetPort: 5432
```
  
Стартуем:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber13.1_apply_prod.png)   


### Как оформить ДЗ?

Выполненное домашнее задание пришлите ссылкой на .md-файл в вашем репозитории.

В качестве решения прикрепите к ДЗ конфиг файлы для деплоя. Прикрепите скриншоты вывода команды kubectl со списком запущенных объектов каждого типа (pods, deployments, statefulset, service) или скриншот из самого Kubernetes, что сервисы подняты и работают.

---

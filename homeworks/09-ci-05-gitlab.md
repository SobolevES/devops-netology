### Подготовка к выполнению  
1. Необходимо зарегистрироваться  
2. Создайте свой новый проект  
3. Создайте новый репозиторий в gitlab, наполните его файлами  
4. Проект должен быть публичным, остальные настройки по желанию  
  
### Основная часть  
  
#### DevOps  
  
В репозитории содержится код проекта на python. Проект - RESTful API сервис. Ваша задача автоматизировать сборку образа с выполнением python-скрипта:  

1. Образ собирается на основе `centos:7`  
2. Python версии не ниже 3.7  
3. Установлены зависимости: `flask flask-jsonpify flask-restful`  
4. Создана директория `/python_api`  
5. Скрипт из репозитория размещён в `/python_api`  
6. Точка вызова: запуск скрипта  
7. Если сборка происходит на ветке master: Образ должен пушится в docker registry вашего gitlab python-api:latest, иначе этот шаг нужно пропустить  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/1_gitlab.JPG)  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/2_gitlab.JPG)  
  
### Product Owner  
  
Вашему проекту нужна бизнесовая доработка: необходимо поменять JSON ответа на вызов метода `GET /rest/api/get_info`, необходимо создать Issue в котором указать:  
  
1. Какой метод необходимо исправить  
2. Текст с `{ "message": "Already started" }` на `{ "message": "Running"}`  
3. Issue поставить `label: feature`  
  
### Developer  
  
Вам пришел новый Issue на доработку, вам необходимо:
  
1. Создать отдельную ветку, связанную с этим `issue`  
2. Внести изменения по тексту из задания  
3. Подготовить `Merge Requst`, влить необходимые изменения в master, проверить, что сборка прошла успешно  
  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/5_gitlab.JPG)  
  
  
### Tester  
  
Разработчики выполнили новый Issue, необходимо проверить валидность изменений:  

1. Поднять докер-контейнер с образом python-api:latest и проверить возврат метода на корректность  
2. Закрыть Issue с комментарием об успешности прохождения, указав желаемый результат и фактически достигнутый  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/6_gitlab.JPG)  
  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/7_gitlab.JPG)  
  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/3_gitlab.JPG)  
  
  
### Итог  
  
После успешного прохождения всех ролей - отправьте ссылку на ваш проект в гитлаб, как решение домашнего задания  
  
  
https://gitlab.com/SobolevES/09-ci-05-gitlab
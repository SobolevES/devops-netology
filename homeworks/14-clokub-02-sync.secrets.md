# Домашнее задание к занятию "14.2 Синхронизация секретов с внешними сервисами. Vault"

## Задача 1: Работа с модулем Vault

Запустить модуль Vault конфигураций через утилиту kubectl в установленном minikube

```
kubectl apply -f 14.2/vault-pod.yml
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.2-apply.png)  
  
Получить значение внутреннего IP пода

```
kubectl get pod 14.2-netology-vault -o json | jq -c '.status.podIPs'
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.2-install.JQ.png)  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.2-podIP.png)  
  

Примечание: jq - утилита для работы с JSON в командной строке

Запустить второй модуль для использования в качестве клиента

```
kubectl run -i --tty fedora --image=fedora --restart=Never -- sh
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.2-docker.run.png)  
  
Установить дополнительные пакеты

```
dnf -y install pip
pip install hvac
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.2-install.hvac1.png)  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.2-install.pip.png)  
  
Запустить интепретатор Python и выполнить следующий код, предварительно
поменяв IP и токен

```
import hvac
client = hvac.Client(
    url='http://10.10.133.71:8200',
    token='aiphohTaa0eeHei'
)
client.is_authenticated()

# Пишем секрет
client.secrets.kv.v2.create_or_update_secret(
    path='hvac',
    secret=dict(netology='Big secret!!!'),
)

# Читаем секрет
client.secrets.kv.v2.read_secret_version(
    path='hvac',
)
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.2-py3.script.png)  
  
## Задача 2 (*): Работа с секретами внутри модуля

* На основе образа fedora создать модуль;
* Создать секрет, в котором будет указан токен;
* Подключить секрет к модулю;
* Запустить модуль и проверить доступность сервиса Vault.


-----
Написал манифест:
```yaml
---
apiVersion: v1
kind: Pod
metadata:
  name: 14.2-netology-fedora
spec:
  containers:
  - name: fedora
    image: fedora
    # Just sleep forever
    command: [ "sleep" ]
    args: [ "infinity" ]
    ports:
    - containerPort: 8200
      protocol: TCP
    env:
    - name: token
      valueFrom:
        secretKeyRef:
          name: 14.2-netology-secret
          key: token

---
apiVersion: v1
kind: Secret
metadata:
  name: 14.2-netology-secret
type: Opaque
stringData:
  token: "rand0mTOKEN"
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.2-fedora.apply2.png)  
  
---

### Как оформить ДЗ?

Выполненное домашнее задание пришлите ссылкой на .md-файл в вашем репозитории.

В качестве решения прикрепите к ДЗ конфиг файлы для деплоя. Прикрепите скриншоты вывода команды kubectl со списком запущенных объектов каждого типа (pods, deployments, statefulset, service) или скриншот из самого Kubernetes, что сервисы подняты и работают, а также вывод из CLI.

---

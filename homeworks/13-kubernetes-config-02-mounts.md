# Домашнее задание к занятию "13.2 разделы и монтирование"
Приложение запущено и работает, но время от времени появляется необходимость передавать между бекендами данные. А сам бекенд генерирует статику для фронта. Нужно оптимизировать это.
Для настройки NFS сервера можно воспользоваться следующей инструкцией (производить под пользователем на сервере, у которого есть доступ до kubectl):
* установить helm: curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
* добавить репозиторий чартов: helm repo add stable https://charts.helm.sh/stable && helm repo update
* установить nfs-server через helm: helm install nfs-server stable/nfs-server-provisioner

В конце установки будет выдан пример создания PVC для этого сервера.  
  
___
### Решение:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber13.2_podgotovka.png)   
   
___

## Задание 1: подключить для тестового конфига общую папку
В stage окружении часто возникает необходимость отдавать статику бекенда сразу фронтом. Проще всего сделать это через общую папку. Требования:
* в поде подключена общая папка между контейнерами (например, /static);
* после записи чего-либо в контейнере с беком файлы можно получить из контейнера с фронтом.
  
___
### Решение:  
  
Взял манифест для stage из предыдещей [ДЗ](https://gitlab.com/SobolevES/devops-netology/-/blob/main/homeworks/13-kubernetes-config-01-objects.md) , доработал его и вот что получилось:  
  
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: front-back
  labels:
    app: front-back
spec:
  replicas: 1
  selector:
    matchLabels:
      app: front-back
  template:
    metadata:
      labels:
        app: front-back
    spec:
      containers:
      - name: frontend
        image: soboleves/homework-fronend:stage
        ports:
        - containerPort: 80
        volumeMounts:
        - mountPath: /share
          name: share

      - name: backend
        image: soboleves/homework-backend:stage
        ports:
        - containerPort: 9000
        volumeMounts:
        - mountPath: /share
          name: share

      volumes:
      - name: share
        emptyDir: {}
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: db
spec:
  selector:
    matchLabels:
      app: db
  serviceName: db
  replicas: 1
  template:
    metadata:
      labels:
        app: db
    spec:
      terminationGracePeriodSeconds: 10
      containers:
      - name: db
        image: postgres:13-alpine
        ports:
        - containerPort: 5432
        env:
          - name: POSTGRES_PASSWORD
            value: postgres
          - name: POSTGRES_USER
            value: postgres
          - name: POSTGRES_DB
            value: news
---
apiVersion: v1
kind: Service
metadata:
  name: db
spec:
  selector:
    app: db
  ports:
    - protocol: TCP
      port: 5432
      targetPort: 5432
```
  
Стартуем и проверяем как все работает:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber13.2_stage_apply_mount.png)   
  
**Все ОК!**    
___

## Задание 2: подключить общую папку для прода
Поработав на stage, доработки нужно отправить на прод. В продуктиве у нас контейнеры крутятся в разных подах, поэтому потребуется PV и связь через PVC. Сам PV должен быть связан с NFS сервером. Требования:
* все бекенды подключаются к одному PV в режиме ReadWriteMany;
* фронтенды тоже подключаются к этому же PV с таким же режимом;
* файлы, созданные бекендом, должны быть доступны фронту.
  
___
### Решение:  
  
Аналогично, берем манифест с прошлой ДЗ для прода (ссылка на ДЗ выше) и правим его:  
  
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: frontend
  labels:
    app: frontend
spec:
  replicas: 1
  selector:
    matchLabels:
      app: frontend
  template:
    metadata:
      labels:
        app: frontend
    spec:
      containers:
      - name: frontend
        image: soboleves/homework-frontend:prod
        ports:
        - containerPort: 80
        env:
          - name: BASE_URL
            value: http://homework-application-prod:9000
        volumeMounts:
        - mountPath: /share
          name: pv-share
      volumes:
      - name: pv-share
        persistentVolumeClaim:
          claimName: pvc-share
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: backend
  labels:
    app: backend
spec:
  replicas: 1
  selector:
    matchLabels:
      app: backend
  template:
    metadata:
      labels:
        app: backend
    spec:
      containers:
      - name: backend
        image: soboleves/homework-backend:prod
        ports:
        - containerPort: 9000
        env:
          - name: DATABASE_URL
            value: postgres://postgres:postgres@application-db-prod:5432/news
        volumeMounts:
        - mountPath: /share
          name: pv-share
      volumes:
      - name: pv-share
        persistentVolumeClaim:
          claimName: pvc-share

---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: db
spec:
  selector:
    matchLabels:
      app: db
  serviceName: "db"
  replicas: 1
  template:
    metadata:
      labels:
        app: db
    spec:
      terminationGracePeriodSeconds: 10
      containers:
      - name: db
        image: postgres:13-alpine
        ports:
        - containerPort: 5432
        env:
          - name: POSTGRES_PASSWORD
            value: postgres
          - name: POSTGRES_USER
            value: postgres
          - name: POSTGRES_DB
            value: news
---
apiVersion: v1
kind: Service
metadata:
  name: frontend
spec:
  selector:
    app: frontend
  ports:
    - protocol: TCP
      port: 8000
      targetPort: 80
---
apiVersion: v1
kind: Service
metadata:
  name: backend
spec:
  selector:
    app: backend
  ports:
    - protocol: TCP
      port: 9000
      targetPort: 9000
---
apiVersion: v1
kind: Service
metadata:
  name: db
spec:
  selector:
    app: db
  ports:
    - protocol: TCP
      port: 5432
      targetPort: 5432
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-share
spec:
  storageClassName: nfs
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 1Gi
```
  
Применяем и проверяем, что все сущности стартанули:  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber13.2_prod_apply_mount.png)  

Долго стартовало, но все ОК!  
  
Приступаем к проверке примаунченой общей шары:  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber13.2_testing.png)  

___
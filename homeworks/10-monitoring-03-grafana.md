## Задание 1

Используя директорию help внутри данного домашнего задания - запустите связку prometheus-grafana.

Зайдите в веб-интерфейс графана, используя авторизационные данные, указанные в манифесте docker-compose.

Подключите поднятый вами prometheus как источник данных.

Решение домашнего задания - скриншот веб-интерфейса grafana со списком подключенных Datasource.

___  
### *Решение:*  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/monitoring3_1.png)  
  
___
## Задание 2

Изучите самостоятельно ресурсы:

+ promql-for-humans
+ understanding prometheus cpu metrics

Создайте Dashboard и в ней создайте следующие Panels:

+ Утилизация CPU для nodeexporter (в процентах, 100-idle)
+ CPULA 1/5/15
+ Количество свободной оперативной памяти
+ Количество места на файловой системе

Для решения данного ДЗ приведите promql запросы для выдачи этих метрик, а также скриншот получившейся Dashboard.
  
___
### *Решение:*  
**Утилизация CPU для nodeexporter (в процентах, 100-idle):**  
```
(1 - avg by(instance)(irate(node_cpu_seconds_total{instance="nodeexporter:9100", job="nodeexporter", mode="idle"}[5m]))) * 100
```
  
**CPULA 1/5/15:**  
```
avg(node_load1{instance="nodeexporter:9100", job="nodeexporter"}) / count(count(node_cpu_seconds_total{instance="nodeexporter:9100", job="nodeexporter"}) by (cpu)) * 100
avg(node_load5{instance="nodeexporter:9100", job="nodeexporter"}) / count(count(node_cpu_seconds_total{instance="nodeexporter:9100", job="nodeexporter"}) by (cpu)) * 100 
avg(node_load15{instance="nodeexporter:9100", job="nodeexporter"}) / count(count(node_cpu_seconds_total{instance="nodeexporter:9100", job="nodeexporter"}) by (cpu)) * 100
```
  
**Количество свободной оперативной памяти:**  
  
```
node_memory_MemFree_bytes{instance="nodeexporter:9100", job="nodeexporter"}/1024^2
```
  
**Количество места на файловой системе:**
  
```
node_filesystem_avail_bytes{instance="nodeexporter:9100", job="nodeexporter", mountpoint="/",fstype!="tmpfs"}/1024^3
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/monitoring3_2.png)  
  
___  
  
## Задание 3

Создайте для каждой Dashboard подходящее правило alert (можно обратиться к первой лекции в блоке "Мониторинг").

Для решения ДЗ - приведите скриншот вашей итоговой Dashboard.
  
___  
  
### *Решение:*  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/monitoring3_3.png)  
  
___  
  
## Задание 4

Сохраните ваш Dashboard.

Для этого перейдите в настройки Dashboard, выберите в боковом меню "JSON MODEL".

Далее скопируйте отображаемое json-содержимое в отдельный файл и сохраните его.


В решении задания - приведите листинг этого файла.  
  
___
  
### *Решение:*  
  
https://gitlab.com/SobolevES/devops-netology/-/tree/main/attach/JSON_model.json  
  
___
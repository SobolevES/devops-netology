# Домашнее задание к занятию 15.2 "Вычислительные мощности. Балансировщики нагрузки".
Домашнее задание будет состоять из обязательной части, которую необходимо выполнить на провайдере Яндекс.Облако, и дополнительной части в AWS (можно выполнить по желанию). Все домашние задания в 15 блоке связаны друг с другом и в конце представляют пример законченной инфраструктуры.
Все задания требуется выполнить с помощью Terraform, результатом выполненного домашнего задания будет код в репозитории. Перед началом работ следует настроить доступ до облачных ресурсов из Terraform, используя материалы прошлых лекций и ДЗ.

---
## Задание 1. Яндекс.Облако (обязательное к выполнению)

1. Создать bucket Object Storage и разместить там файл с картинкой:
- Создать bucket в Object Storage с произвольным именем (например, _имя_студента_дата_);
- Положить в bucket файл с картинкой;
- Сделать файл доступным из Интернет.
2. Создать группу ВМ в public подсети фиксированного размера с шаблоном LAMP и web-страничкой, содержащей ссылку на картинку из bucket:
- Создать Instance Group с 3 ВМ и шаблоном LAMP. Для LAMP рекомендуется использовать `image_id = fd827b91d99psvq5fjit`;
- Для создания стартовой веб-страницы рекомендуется использовать раздел `user_data` в [meta_data](https://cloud.yandex.ru/docs/compute/concepts/vm-metadata);
- Разместить в стартовой веб-странице шаблонной ВМ ссылку на картинку из bucket;
- Настроить проверку состояния ВМ.
3. Подключить группу к сетевому балансировщику:
- Создать сетевой балансировщик;
- Проверить работоспособность, удалив одну или несколько ВМ.
4. *Создать Application Load Balancer с использованием Instance group и проверкой состояния.

Документация
- [Compute instance group](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/compute_instance_group)
- [Network Load Balancer](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/lb_network_load_balancer)
- [Группа ВМ с сетевым балансировщиком](https://cloud.yandex.ru/docs/compute/operations/instance-groups/create-with-balancer)

### Решение:  

Получился вот такой конфиг:  
```terraform
terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.68.0"
    }
  }
}

provider "yandex" {
  token     = "***"
  cloud_id  = "***"
  folder_id = "***"
  zone      = "ru-central1-a"
}

locals {
  folder_id = "***"
}

resource "yandex_iam_service_account" "sa" {
  folder_id = local.folder_id
  name      = "account"
}

resource "yandex_resourcemanager_folder_iam_member" "sa-admin" {
  folder_id = local.folder_id
  role      = "admin"
  member    = "serviceAccount:${yandex_iam_service_account.sa.id}"
}

resource "yandex_iam_service_account_static_access_key" "sa-static-key" {
  service_account_id = yandex_iam_service_account.sa.id
  description        = "static access key for object storage"
}

resource "yandex_resourcemanager_folder_iam_member" "sa-edit1" {
  folder_id = local.folder_id
  role      = "editor"
  member    = "serviceAccount:${yandex_iam_service_account.sa.id}"
}

resource "yandex_storage_bucket" "ysbucket" {
  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  bucket = "evgen-bucket"
}

resource "yandex_storage_object" "storage-pic" {
  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  bucket = yandex_storage_bucket.ysbucket.id
  key    = "nirvana.jpeg"
  source = "./nirvana.jpeg"
  acl    = "public-read"
}

resource "yandex_vpc_network" "vpc-1" {
  name = "vpc-network1"
}

resource "yandex_vpc_subnet" "public-subnet" {
  name           = "public-subnet"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.vpc-1.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

resource "yandex_vpc_subnet" "public-subnet2" {
  name           = "public-subnet2"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.vpc-1.id
  v4_cidr_blocks = ["192.168.30.0/24"]
}

resource "yandex_compute_instance_group" "group-web" {
  name                = "web-group"
  folder_id           = local.folder_id
  service_account_id  = yandex_iam_service_account.sa.id
  deletion_protection = false

    allocation_policy {
    zones = ["ru-central1-a", "ru-central1-b"]
  }

  scale_policy {
    fixed_scale {
      size = 3
    }
  }

  deploy_policy {
    max_unavailable = 1
    max_creating    = 3
    max_expansion   = 1
    max_deleting    = 1
  }


  instance_template {
    platform_id = "standard-v1"
    service_account_id  = yandex_iam_service_account.sa.id

    resources {
      memory = 1
      cores  = 2
      core_fraction = 5
    }

    boot_disk {
      mode = "READ_WRITE"
      initialize_params {
        image_id = "fd827b91d99psvq5fjit"
        size     = 4
      }
    }

    scheduling_policy {
      preemptible = true
    }

    network_interface {
      network_id = yandex_vpc_network.vpc-1.id
      subnet_ids = [yandex_vpc_subnet.public-subnet.id, yandex_vpc_subnet.public-subnet2.id]
      nat        = true
    }

    metadata = {
      user-data = file("./bootstrap.sh")
      ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
    }
    network_settings {
      type = "STANDARD"
    }
  }

  health_check {
    interval = 60
    timeout = 2
    tcp_options {
      port = 80
    }
  }

  load_balancer {
    target_group_name = "web-target-group"
  }
  
  depends_on = [yandex_resourcemanager_folder_iam_member.sa-edit1]
}


resource "yandex_lb_network_load_balancer" "web" {
  name = "web-network-load-balancer"

  listener {
    name = "web-listener"
    port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_compute_instance_group.group-web.load_balancer[0].target_group_id

    healthcheck {
      name = "http"
      http_options {
        port = 80
        path = ""
      }
    }
  }
}
```
  
плюс [bootstrap.sh](https://gitlab.com/SobolevES/devops-netology/-/tree/main/attach/bootstrap.sh)
  
  
Применяем:  
```
terraform apply
```
  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber15.2-dashboard.png)  
  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber15.2-pic.png)  
  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber15.2-list_VM.png)  
  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber15.2-web-group0.png)  
  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber15.2-web-group1.png)  
  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber15.2-web-group2.png)  
  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber15.2-LB-web-target-group.png)  
  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber15.2-status_LB.png)  
  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber15.2-LB-1unhealth1.png)  
  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber15.2-LB-1unhealth2.png)  
  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber15.2-pic_in_ui.png)  
  
Удаляем:  
```
terraform destroy
```
  
---
## Задание 2*. AWS (необязательное к выполнению)

Используя конфигурации, выполненные в рамках ДЗ на предыдущем занятии, добавить к Production like сети Autoscaling group из 3 EC2-инстансов с  автоматической установкой web-сервера в private домен.

1. Создать bucket S3 и разместить там файл с картинкой:
- Создать bucket в S3 с произвольным именем (например, _имя_студента_дата_);
- Положить в bucket файл с картинкой;
- Сделать доступным из Интернета.
2. Сделать Launch configurations с использованием bootstrap скрипта с созданием веб-странички на которой будет ссылка на картинку в S3. 
3. Загрузить 3 ЕС2-инстанса и настроить LB с помощью Autoscaling Group.

Resource terraform
- [S3 bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket)
- [Launch Template](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/launch_template)
- [Autoscaling group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_group)
- [Launch configuration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/launch_configuration)

Пример bootstrap-скрипта:
```
#!/bin/bash
yum install httpd -y
service httpd start
chkconfig httpd on
cd /var/www/html
echo "<html><h1>My cool web-server</h1></html>" > index.html
```

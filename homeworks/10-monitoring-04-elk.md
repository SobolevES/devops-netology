# Домашнее задание к занятию "10.04. ELK"

## Дополнительные ссылки

При выполнении задания пользуйтесь вспомогательными ресурсами:

- [поднимаем elk в докер](https://www.elastic.co/guide/en/elastic-stack-get-started/current/get-started-docker.html)
- [поднимаем elk в докер с filebeat и докер логами](https://www.sarulabs.com/post/5/2019-08-12/sending-docker-logs-to-elasticsearch-and-kibana-with-filebeat.html)
- [конфигурируем logstash](https://www.elastic.co/guide/en/logstash/current/configuration.html)
- [плагины filter для logstash](https://www.elastic.co/guide/en/logstash/current/filter-plugins.html)
- [конфигурируем filebeat](https://www.elastic.co/guide/en/beats/libbeat/5.3/config-file-format.html)
- [привязываем индексы из elastic в kibana](https://www.elastic.co/guide/en/kibana/current/index-patterns.html)
- [как просматривать логи в kibana](https://www.elastic.co/guide/en/kibana/current/discover.html)
- [решение ошибки increase vm.max_map_count elasticsearch](https://stackoverflow.com/questions/42889241/how-to-increase-vm-max-map-count)

В процессе выполнения задания могут возникнуть также не указанные тут проблемы в зависимости от системы.

Используйте output stdout filebeat/kibana и api elasticsearch для изучения корня проблемы и ее устранения.

## Задание повышенной сложности

Не используйте директорию [help](./help) при выполнении домашнего задания.

## Задание 1

Вам необходимо поднять в докере:
- elasticsearch(hot и warm ноды)
- logstash
- kibana
- filebeat

и связать их между собой.

Logstash следует сконфигурировать для приёма по tcp json сообщений.

Filebeat следует сконфигурировать для отправки логов docker вашей системы в logstash.

В директории [help](./help) находится манифест docker-compose и конфигурации filebeat/logstash для быстрого 
выполнения данного задания.

Результатом выполнения данного задания должны быть:
- скриншот `docker ps` через 5 минут после старта всех контейнеров (их должно быть 5)
- скриншот интерфейса kibana
- docker-compose манифест (если вы не использовали директорию help)
- ваши yml конфигурации для стека (если вы не использовали директорию help)

=============================================
### **Решение:**
  
Итак, с какими проблемами я столкнулся при работе с подготовленными материалами из [help](./help) :  
1. это наверное, как и большинство студентов, выполняющих это дз, с нехваткой MEM для эластика. Но тут все просто, в логах все видно, что нужно править.  
```
sudo sysctl -w vm.max_map_count=262144
sudo systemctl restart docker
```
2. filebeat не понятно почему не хотел ни в какую искать сеть, добавил новую для логстеша и файлбита.  
3. в манифесте, в конфиге логстеша изменил точку монтирования на /usr/share/logstash/pipeline/  
4. в logstash.conf был блок filter с пустыми параметрами, попробовал с ними поиграться, в итоге вообще закомментил  
5. Вроде бы ничего, в кибане начали прилетать логи, но в какой то адовой кодировке, пробовал в конфигах прописывать кодировку, но не помогало. В итоге изменил в блоке input  `tcp`  на `beats`  
6. Столкнулся с проблемой, что в наименовании индекса перестала работать конструкция `{[@metadata][indexDate]}` . нагуглил, что провайдер `beats` не использует поле `[indexDate]`. Заменил на `{+yyyy.MM.dd}`. 
[Тут](https://www.elastic.co/guide/en/logstash/current/event-dependent-configuration.html) нагуглил на что можно поменять.  
  
  
  
**Работа контейнеров спустя 8-9мин:**  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/elk2.png)  
  
**UI Kibana:**  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/elk1.png)  
  
**Проблема с кодировкой, о которой написал в п.5 выше:**  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/elk3.png)  
  
**Индексы в эластике:**  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/elk4.png)  
  
**Тренировка по созданию индексов в кибане:**  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/elk5.png)  


**Финальные файлы конфигов и манифеста, которые в итоге получились:**  
[docker-compose.yml](https://gitlab.com/SobolevES/devops-netology/-/raw/main/attach/docker-compose.yml)  
[logstash.conf](https://gitlab.com/SobolevES/devops-netology/-/raw/main/attach/logstash.conf)  
[filebeat.conf](https://gitlab.com/SobolevES/devops-netology/-/raw/main/attach/filebeat.yml)  
[logstash.yml](https://gitlab.com/SobolevES/devops-netology/-/raw/main/attach/logstash.yml)  

=============================================
  
## Задание 2

Перейдите в меню [создания index-patterns  в kibana](http://localhost:5601/app/management/kibana/indexPatterns/create)
и создайте несколько index-patterns из имеющихся.

Перейдите в меню просмотра логов в kibana (Discover) и самостоятельно изучите как отображаются логи и как производить 
поиск по логам.

В манифесте директории help также приведенно dummy приложение, которое генерирует рандомные события в stdout контейнера.
Данные логи должны порождать индекс logstash-* в elasticsearch. Если данного индекса нет - воспользуйтесь советами 
и источниками из раздела "Дополнительные ссылки" данного ДЗ.
 
  
=============================================
### **Решение:**
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/elk6.png)  
---

### Как оформить ДЗ?

Выполненное домашнее задание пришлите ссылкой на .md-файл в вашем репозитории.

---

 

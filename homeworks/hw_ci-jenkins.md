##**Подготовка к выполнению**  
>1.Установить jenkins по любой из инструкций  
2.Запустить и проверить работоспособность  
3.Сделать первоначальную настройку  
4.Настроить под свои нужды  
5.Поднять отдельный cloud  
6.Для динамических агентов можно использовать образ  
7.Обязательный параметр: поставить label для динамических агентов: ansible_docker  
8.Сделать форк репозитория с playbook  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/1.2_jenkins.JPG)  
  
##**Основная часть**  

>1.Сделать Freestyle Job, который будет запускать ansible-playbook из форка репозитория  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/1.0_jenkins.JPG)  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/1.1_jenkins.JPG)  
  
**Результат выполнения фристайл джоба:**  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/1.3_jenkins.JPG)  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/1.3.2_jenkins.JPG)  
  
>2.Сделать Declarative Pipeline, который будет выкачивать репозиторий с плейбукой и запускать её  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/1.4_jenkins.JPG)  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/1.5_jenkins.JPG)  
  
>3.Перенести Declarative Pipeline в репозиторий в файл Jenkinsfile  
>4.Перенастроить Job на использование Jenkinsfile из репозитория  
>5.Создать Scripted Pipeline, наполнить его скриптом из pipeline  
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/1.6_jenkins.JPG)  
  
>6.Заменить credentialsId на свой собственный  
>7.Проверить работоспособность, исправить ошибки, исправленный Pipeline вложить в репозитрий в файл ScriptedJenkinsfile  
>8.Отправить ссылку на репозиторий в ответе  
  
  
https://github.com/sobolevES/example-playbook
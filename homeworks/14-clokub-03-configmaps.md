# Домашнее задание к занятию "14.3 Карты конфигураций"

## Задача 1: Работа с картами конфигураций через утилиту kubectl в установленном minikube

Выполните приведённые команды в консоли. Получите вывод команд. Сохраните
задачу 1 как справочный материал.

### Как создать карту конфигураций?

```
kubectl create configmap nginx-config --from-file=nginx.conf
kubectl create configmap domain --from-literal=name=netology.ru
```

![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.3-create.png)  
  
### Как просмотреть список карт конфигураций?

```
kubectl get configmaps
kubectl get configmap
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.3-get.configmap.png)  
  
### Как просмотреть карту конфигурации?

```
kubectl get configmap nginx-config
kubectl describe configmap domain
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.3-descr.png)  
  
### Как получить информацию в формате YAML и/или JSON?

```
kubectl get configmap nginx-config -o yaml
kubectl get configmap domain -o json
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.3-yaml-json.png)  
  
### Как выгрузить карту конфигурации и сохранить его в файл?

```
kubectl get configmaps -o json > configmaps.json
kubectl get configmap nginx-config -o yaml > nginx-config.yml
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.3>.png)  
  
### Как удалить карту конфигурации?

```
kubectl delete configmap nginx-config
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.3-delete.png)  
  
### Как загрузить карту конфигурации из файла?

```
kubectl apply -f nginx-config.yml
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.3-apply.png)  
  
## Задача 2 (*): Работа с картами конфигураций внутри модуля

Выбрать любимый образ контейнера, подключить карты конфигураций и проверить
их доступность как в виде переменных окружения, так и в виде примонтированного
тома
  
---
## Решение:  
  
Взял манифест с прошлой дз и доработал его:  
  
```yaml
---
apiVersion: v1
kind: Pod
metadata:
  name: 14.3-netology-fedora
spec:
  containers:
  - name: fedora
    image: fedora
    # Just sleep forever
    command: [ "sleep" ]
    args: [ "infinity" ]
    env:
      - name: DOMAIN_CONFIGMAP
        valueFrom:
          configMapKeyRef:
            name: nginx-config
            key: conf.conf
    envFrom:
      - configMapRef:
          name: nginx-config
    ports:
    - containerPort: 80
    volumeMounts:
    - mountPath: /opt/conf.conf
      subPath: conf.conf
      name: vlm-4-conf
  volumes:
  - name: vlm-4-conf
    configMap:
      name: nginx-config
```
  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/kuber14.3-2*.png)  
  
---

### Как оформить ДЗ?

Выполненное домашнее задание пришлите ссылкой на .md-файл в вашем репозитории.

В качестве решения прикрепите к ДЗ конфиг файлы для деплоя. Прикрепите скриншоты вывода команды kubectl со списком запущенных объектов каждого типа (pods, deployments, configmaps) или скриншот из самого Kubernetes, что сервисы подняты и работают, а также вывод из CLI.

---

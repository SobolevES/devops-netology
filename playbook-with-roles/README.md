**Задача: Установить Java, Elastic, Kibana через roles**

Установка зависимостей:
```yaml
ansible-galaxy install -r requirements.yml
```

Запуск плейбука:  
  
```yaml
ansible-playbook -i inventory/prod.yml --ask-pass site.yml
```


    
репа с roles для elastic:  
https://github.com/sobolevES/elastic-role
  
репа с roles для kibana:  
https://github.com/sobolevES/kibana-role

плейбук:  
https://gitlab.com/SobolevES/devops-netology/-/tree/main/playbook-with-roles
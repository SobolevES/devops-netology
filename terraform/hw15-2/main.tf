terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.68.0"
    }
  }
}

provider "yandex" {
  token     = "AQAAAABUICgtAATuwURITfng8EmugOWJw_P6ZIw"
  cloud_id  = "b1g01sq7smcecluonu28"
  folder_id = "b1gatq2ngdl5274j8s1r"
  zone      = "ru-central1-a"
}

locals {
  folder_id = "b1gatq2ngdl5274j8s1r"
}

// Serv acc
resource "yandex_iam_service_account" "sa" {
  folder_id = local.folder_id
  name      = "account"
}

// Role
resource "yandex_resourcemanager_folder_iam_member" "sa-admin" {
  folder_id = local.folder_id
  role      = "admin"
  member    = "serviceAccount:${yandex_iam_service_account.sa.id}"
}

// Create static key
resource "yandex_iam_service_account_static_access_key" "sa-static-key" {
  service_account_id = yandex_iam_service_account.sa.id
  description        = "static access key for object storage"
}


// Grant permissions
resource "yandex_resourcemanager_folder_iam_member" "sa-edit1" {
  folder_id = local.folder_id
  role      = "editor"
  member    = "serviceAccount:${yandex_iam_service_account.sa.id}"
}

// Use keys to create bucket
resource "yandex_storage_bucket" "ysbucket" {
  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  bucket = "evgen-bucket"
  # acl    = "public-read"
}

// Upload file to bucket
resource "yandex_storage_object" "picture1" {
  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  bucket = yandex_storage_bucket.ysbucket.id
  key    = "nirvana.jpeg"
  source = "./nirvana.jpeg"
  acl    = "public-read"
}

resource "yandex_vpc_network" "vpc-1" {
  name = "vpc-network1"
}

resource "yandex_vpc_subnet" "public-subnet" {
  name           = "public-subnet"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.vpc-1.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

resource "yandex_vpc_subnet" "public-subnet2" {
  name           = "public-subnet2"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.vpc-1.id
  v4_cidr_blocks = ["192.168.30.0/24"]
}


// Create Instance Group
resource "yandex_compute_instance_group" "group-web" {
  name                = "web-group"
  folder_id           = local.folder_id
  service_account_id  = yandex_iam_service_account.sa.id
  deletion_protection = false

    allocation_policy {
    zones = ["ru-central1-a", "ru-central1-b"]
  }

  scale_policy {
    fixed_scale {
      size = 3
    }
  }

  deploy_policy {
    max_unavailable = 1
    max_creating    = 3
    max_expansion   = 1
    max_deleting    = 1
  }


  instance_template {
    platform_id = "standard-v1"
    service_account_id  = yandex_iam_service_account.sa.id

    resources {
      memory = 1
      cores  = 2
      core_fraction = 5
    }

    boot_disk {
      mode = "READ_WRITE"
      initialize_params {
        image_id = "fd827b91d99psvq5fjit"
        size     = 4
      }
    }

    scheduling_policy {
      preemptible = true #
    }

    network_interface {
      network_id = yandex_vpc_network.vpc-1.id
      subnet_ids = [yandex_vpc_subnet.public-subnet.id, yandex_vpc_subnet.public-subnet2.id]
      nat        = true
    }

    metadata = {
      user-data = file("./bootstrap.sh")
      ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
    }
    network_settings {
      type = "STANDARD"
    }
  }
  // Instances health checking
  health_check {
    interval = 60
    timeout = 2
    tcp_options {
      port = 80
    }
  }

  load_balancer {
    target_group_name = "web-target-group"
  }

  # application_load_balancer =
  depends_on = [yandex_resourcemanager_folder_iam_member.sa-edit1]
}


resource "yandex_lb_network_load_balancer" "web" {
  name = "web-network-load-balancer"

  listener {
    name = "web-listener"
    port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_compute_instance_group.group-web.load_balancer[0].target_group_id

    healthcheck {
      name = "http"
      http_options {
        port = 80
        path = ""
      }
    }
  }
}